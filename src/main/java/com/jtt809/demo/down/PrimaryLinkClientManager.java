package com.jtt809.demo.down;

import cn.hutool.core.util.RandomUtil;
import com.jtt809.demo.down.handler.initializer.ClientJtt809Initializer;
import com.jtt809.demo.down.pojo.command.request.*;
import com.jtt809.demo.down.pojo.command.response.ResponseClientJtt809_0x1002;
import com.jtt809.demo.up.config.Jtt809Config;
import com.jtt809.demo.up.pojo.Location;
import com.jtt809.demo.up.util.MockUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * 主链路
 * 下级平台客户端
 */
@Data
@Slf4j
public class PrimaryLinkClientManager implements Runnable {

    private static final short ENCRYPT_FLAG = 1;
    private static final String VEHICLE_NO_TEST = "沪D29M2H";

    private Bootstrap bootstrap;

    private Channel channel;

    private String ip;

    private int port;

    private String downLinkIp;

    private int downLinkPort;

    public PrimaryLinkClientManager(String ip, int port, String downLinkIp, int downLinkPort) {
        this.ip = ip;
        this.port = port;
        this.downLinkIp = downLinkIp;
        this.downLinkPort = downLinkPort;
    }

    public void start() {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try {
            // 同上级平台进行连接
            bootstrap = new Bootstrap();
            bootstrap.group(eventLoopGroup)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    //读缓冲区为nM
                    .option(ChannelOption.SO_RCVBUF, 10 * 1024 * 1024)
                    // 接收发送数据的缓存大小nM
                    .option(ChannelOption.WRITE_BUFFER_WATER_MARK, new WriteBufferWaterMark(1,10 * 1024 * 1024))
                    .channel(NioSocketChannel.class)
                    .handler(new ClientJtt809Initializer(ip, port, downLinkIp, downLinkPort));

            // 连接服务端
            ChannelFuture channelFuture = bootstrap.connect(ip, port);
            channelFuture.addListener(new ChannelFutureListener() {
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (!future.isSuccess()) {
                        final EventLoop loop = future.channel().eventLoop();
                        loop.schedule(new Runnable() {
                            public void run() {
                                log.info("======> 上级平台连接不上，开始重连操作......");
                                start();
                            }
                        }, 5L, TimeUnit.SECONDS);
                    } else {
                        channel = future.channel();
                        log.info("======> 上级平台连接成功......");
                    }
                }
            });

            Thread.sleep(2000);

            // 发送登录指令
            RequestClientJtt809_0x1001 jtt8090X1001Client = new RequestClientJtt809_0x1001();
            jtt8090X1001Client.setEncryptFlag(ENCRYPT_FLAG);
            jtt8090X1001Client.setEncryptKey(Jtt809Config.JTT809_NETTY_ENCRYPT_KEY);

            jtt8090X1001Client.setUserId(Jtt809Config.JTT809_NETTY_SERVER_DOWN_USERID);
            jtt8090X1001Client.setPassword(Jtt809Config.JTT809_NETTY_SERVER_DOWN_PASSWORD);
            jtt8090X1001Client.setMsgGesscenterId(Jtt809Config.JTT809_FACTORY_ACCESS_CODE);
            jtt8090X1001Client.setDownLinkIp(this.downLinkIp);
            jtt8090X1001Client.setDownLinkPort(this.downLinkPort);
            //channel.writeAndFlush(jtt8090X1001Client.encode());
            channel.writeAndFlush(jtt8090X1001Client);

            Thread.sleep(2000);

            // 登录后，循环发送车辆定位信息
            if (ResponseClientJtt809_0x1002.isIsLoginFlagFromUpPlatform() && Jtt809Config.JTT809_NETTY_SERVER_DOWN_TEST) {

                for (int i = 0; i < 101; i++) {
//                while (true) {
                    // 等待[0,3000)毫秒
                    //Thread.sleep(RandomUtil.randomInt(500, 2000));

                    RequestClientJtt809_0x1202 requestJtt8090x1202 = new RequestClientJtt809_0x1202();
                    requestJtt8090x1202.setEncryptFlag(ENCRYPT_FLAG);
                    requestJtt8090x1202.setEncryptKey(Jtt809Config.JTT809_NETTY_ENCRYPT_KEY);
                    //requestJtt8090x1202.setVehicleNo(MockUtil.getVechileNo());
                    requestJtt8090x1202.setVehicleNo(VEHICLE_NO_TEST);
                    requestJtt8090x1202.setVehicleColor(RequestClientJtt809_0x1200_VehicleColor.BLUE);

                    requestJtt8090x1202.setLocation(buildLocation());
                    channel.writeAndFlush(requestJtt8090x1202);
                }

                //补发定位信息
//                Thread.sleep(RandomUtil.randomInt(3000, 5000));
//                RequestClientJtt809_0x1203 requestJtt8090x1203 = new RequestClientJtt809_0x1203();
//                requestJtt8090x1203.setEncryptFlag(ENCRYPT_FLAG);
//                requestJtt8090x1203.setEncryptKey(Jtt809Config.JTT809_NETTY_ENCRYPT_KEY);
//                requestJtt8090x1203.setVehicleNo(VEHICLE_NO_TEST);
//                requestJtt8090x1203.setVehicleColor(RequestClientJtt809_0x1200_VehicleColor.BLUE);
//                requestJtt8090x1203.setLocations(Arrays.asList(buildLocation(), buildLocation(), buildLocation()));
//                requestJtt8090x1203.setGpsSize(requestJtt8090x1203.getLocations().size());
//                channel.writeAndFlush(requestJtt8090x1203);
//
//
//                //车辆注册
//                Thread.sleep(RandomUtil.randomInt(3000, 5000));
//                RequestClientJtt809_0x1201 requestJtt8090x1201 = new RequestClientJtt809_0x1201();
//                requestJtt8090x1201.setEncryptFlag(ENCRYPT_FLAG);
//                requestJtt8090x1201.setEncryptKey(Jtt809Config.JTT809_NETTY_ENCRYPT_KEY);
//                requestJtt8090x1201.setVehicleNo(VEHICLE_NO_TEST);
//                requestJtt8090x1201.setVehicleColor(RequestClientJtt809_0x1200_VehicleColor.BLUE);
//                requestJtt8090x1201.setPlatformId(RandomUtil.randomString(6));
//                requestJtt8090x1201.setProducerId(RandomUtil.randomString(6));
//                requestJtt8090x1201.setTerminalModeType(RandomUtil.randomString(6));
//                requestJtt8090x1201.setImeiId(RandomUtil.randomString(6));
//                requestJtt8090x1201.setTerminalId(RandomUtil.randomString(6));
//                requestJtt8090x1201.setTerminalSimCode(RandomUtil.randomString(6));
//                channel.writeAndFlush(requestJtt8090x1201);
//
//
//                //补发车辆静态信息
//                Thread.sleep(RandomUtil.randomInt(3000, 5000));
//                StringBuilder carInfo = new StringBuilder();
//                carInfo.append(RequestClientJtt809_0x1601.KEY_VIN + ":=" + VEHICLE_NO_TEST + ";");
//                carInfo.append(RequestClientJtt809_0x1601.KEY_VEHICLE_COLOR + ":=" + RequestClientJtt809_0x1200_VehicleColor.BLUE+ ";");
//                carInfo.append(RequestClientJtt809_0x1601.KEY_VEHICLE_TYPE + ":=1;");
//                carInfo.append(RequestClientJtt809_0x1601.KEY_TRANS_TYPE + ":=1;");
//                carInfo.append(RequestClientJtt809_0x1601.KEY_VEHICLE_NATIONALITY + ":=1;");
//                carInfo.append(RequestClientJtt809_0x1601.KEY_OWERS_ID + ":=KEY_OWERS_ID;");
//                carInfo.append(RequestClientJtt809_0x1601.KEY_OWERS_NAME + ":=KEY_OWERS_NAME;");
//                carInfo.append(RequestClientJtt809_0x1601.KEY_OWERS_TEL + ":=KEY_OWERS_TEL;");
//
//                RequestClientJtt809_0x1601 requestJtt8090x1601 = new RequestClientJtt809_0x1601(carInfo.toString());
//                requestJtt8090x1601.setEncryptFlag(ENCRYPT_FLAG);
//                requestJtt8090x1601.setEncryptKey(Jtt809Config.JTT809_NETTY_ENCRYPT_KEY);
//                requestJtt8090x1601.setVehicleNo(VEHICLE_NO_TEST);
//                requestJtt8090x1601.setVehicleColor(RequestClientJtt809_0x1200_VehicleColor.BLUE);
//                channel.writeAndFlush(requestJtt8090x1601);
//
//                //上报驾驶员信息
//                Thread.sleep(RandomUtil.randomInt(3000, 5000));
//                RequestClientJtt809_0x120A requestJtt8090x120A = new RequestClientJtt809_0x120A();
//                requestJtt8090x120A.setEncryptFlag(ENCRYPT_FLAG);
//                requestJtt8090x120A.setEncryptKey(Jtt809Config.JTT809_NETTY_ENCRYPT_KEY);
//                requestJtt8090x120A.setVehicleNo(VEHICLE_NO_TEST);
//                requestJtt8090x120A.setVehicleColor(RequestClientJtt809_0x1200_VehicleColor.BLUE);
//                requestJtt8090x120A.setDriverName(RandomUtil.randomString(6));
//                requestJtt8090x120A.setDriverId(RandomUtil.randomString(6));
//                requestJtt8090x120A.setLicence(RandomUtil.randomString(6));
//                requestJtt8090x120A.setOrgName(RandomUtil.randomString(6));
//                requestJtt8090x120A.setSourceDataType((short) 1);
//                requestJtt8090x120A.setSourceMsgSn(1);
//                requestJtt8090x120A.setValidDate(TimeUtil.getUTCTime());
//                channel.writeAndFlush(requestJtt8090x120A);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        start();
    }

    private Location buildLocation() {
        Location location = new Location();
        location.setEncrypt((byte) 0);
        Calendar calendar = Calendar.getInstance();
        location.setYear(calendar.get(Calendar.YEAR));
        location.setMonth(calendar.get(Calendar.MONTH) + 1);
        location.setDay(calendar.get(Calendar.DATE));
        location.setHour(calendar.get(Calendar.HOUR_OF_DAY));
        location.setMinute(calendar.get(Calendar.MINUTE));
        location.setSecond(calendar.get(Calendar.SECOND));
        location.setLon(MockUtil.randomLonLat(117.5675674618, 117.6984245470, 24.9297423670, 25.0475137438, "lon"));
        location.setLat(MockUtil.randomLonLat(117.5675674618, 117.6984245470, 24.9297423670, 25.0475137438, "lat"));
        location.setVec1((short) RandomUtil.randomInt(150));
        location.setVec2((short) RandomUtil.randomInt(150));
        location.setVec3((short) RandomUtil.randomInt(10000));
        location.setDirection((short) RandomUtil.randomInt(0, 360));
        location.setAltitude((short) RandomUtil.randomInt(0, 6000));
        location.setPlatformId1(RandomUtil.randomString(6));
        location.setPlatformId2(RandomUtil.randomString(6));
        location.setPlatformId3(RandomUtil.randomString(6));
        location.setState(1);
        location.setAlarm(1);
        return location;
    }

}
