package com.jtt809.demo.down.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 上报车辆驾驶员身份识别信息应答
 * 子业务类型标识：UP_EXG_MSG_REPORT_DRIVER_INFO_ACK
 * 描述：上报车辆驾驶员身份识别信息应答
 */
@Setter
@Getter
public class RequestClientJtt809_0x120A extends RequestClientJtt809_0x1200_VehiclePackage {

    /**
     * 驾驶员姓名
     */
    private String driverName;

    /**
     * 驾驶员身份证编号
     */
    private String driverId;

    /**
     * 从业资格证号(备用)
     */
    private String licence;

    /**
     * 发证机构名称(备用)
     */
    private String orgName;

    /*2019*/
    /**
     * 上报信息源子业务类型
     */
    private short sourceDataType;
    /**
     * 上报信息源序列号
     */
    private int sourceMsgSn;
    /**
     * 证件有效期，时分秒为0
     */
    private long validDate;
    /*2019*/

    public RequestClientJtt809_0x120A() {
        super(ConstantJtt809.UP_EXG_MSG_REPORT_DRIVER_INFO_ACK);
        if (isJtt809Version2019()) {
            super.setDataLength(290);
        } else {
            super.setDataLength(276);
        }
    }

    @Override
    protected void encodeDataImpl(ByteBuf buf) {
        if (isJtt809Version2019()) {
            //2 byte
            buf.writeShort(getSourceDataType());
            //4 byte
            buf.writeInt(getSourceMsgSn());
        }

        //16 byte
        buf.writeBytes(getBytesWithLengthAfter(16, getDriverName().getBytes()));
        //20 byte
        buf.writeBytes(getBytesWithLengthAfter(20, getDriverId().getBytes()));
        //40 byte
        buf.writeBytes(getBytesWithLengthAfter(40, getLicence().getBytes()));
        //200 byte
        buf.writeBytes(getBytesWithLengthAfter(200, getOrgName().getBytes()));

        if (isJtt809Version2019()) {
            //8 byte
            buf.writeLong(getValidDate());
        }
    }
}
