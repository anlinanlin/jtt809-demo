package com.jtt809.demo.up.constant;

import com.jtt809.demo.up.config.Jtt809Config;

public interface Constant {

    /**
     * 车辆动态信息mq名称
     */
    String JTT809_GPS_QUEUE = "jtt809_gps_queue_source" + Jtt809Config.JTT809_SOURCE;

    /**
     * ip正则表达式
     */
    String REGEX_IS_IP = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";

    /**
     * mysql数据状态---正常
     */
    int DB_STATUS_NORMAL = 1;
}
