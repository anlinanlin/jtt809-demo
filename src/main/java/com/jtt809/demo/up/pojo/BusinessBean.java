package com.jtt809.demo.up.pojo;

import io.netty.channel.ChannelHandlerContext;
import lombok.Data;

/**
 * <p>基础业务类</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 17:13
 */
@Data
public class BusinessBean {
    private ChannelHandlerContext ctx;

    private Response response;

    public BusinessBean(ChannelHandlerContext ctx, Response response) {
        this.ctx = ctx;
        this.response = response;
    }
}
