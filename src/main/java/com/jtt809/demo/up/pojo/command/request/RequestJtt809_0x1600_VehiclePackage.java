package com.jtt809.demo.up.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.Data;

import java.io.UnsupportedEncodingException;

/**
 * 车辆信息基本类
 */
@Data
public abstract class RequestJtt809_0x1600_VehiclePackage extends BasePackage {

    /**
     * 车辆基本信息数据长度
     */
    public static final short VEHICLE_MSG_FIX_LENGTH = 28;

    /**
     * 车牌号
     */
    private String vehicleNo;

    /**
     * 车辆颜色，按照 JT/T415-2006中 5.4.12 的规定
     * 1 - 蓝色
     * 2 - 黄色
     * 3 - 黑色
     * 4 - 白色
     * 9 - 其他
     */
    private byte vehicleColor;

    /**
     * 子业务类型标识
     */
    private short dataType;

    /**
     * 后续数据长度
     */
    protected int dataLength;

    public RequestJtt809_0x1600_VehiclePackage(int dataType) {
        super(ConstantJtt809.DOWN_BASE_MSG);

        this.dataType = (short) dataType;
        this.msgBodyLength = VEHICLE_MSG_FIX_LENGTH + dataLength;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {
        ByteBuf baseBuf = Unpooled.buffer(getMsgBodyLength());
        // 车牌号 21 byte
        try {
            baseBuf.writeBytes(getBytesWithLengthAfter(21, getVehicleNo().getBytes("GBK")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // 车牌颜色：注意不是车身颜色 1 byte
        baseBuf.writeByte(getVehicleColor());
        // 子业务码 2 byte
        baseBuf.writeShort(getDataType());
        // 后续数据长度 4 byte
        baseBuf.writeInt(getDataLength());
        // 具体车辆信息数据体加密啊
        encodeDataImpl(baseBuf);

        // 写入最终数据包
        buf.writeBytes(baseBuf);
    }

    /**
     * 具体车辆数据实现方法
     *
     * @param buf
     */
    protected abstract void encodeDataImpl(ByteBuf buf);

}
