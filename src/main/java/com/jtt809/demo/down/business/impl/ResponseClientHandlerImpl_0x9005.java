package com.jtt809.demo.down.business.impl;

import com.jtt809.demo.down.business.IClientBusinessServer;
import com.jtt809.demo.down.pojo.command.request.RequestClientJtt809_0x9006;
import com.jtt809.demo.down.pojo.command.response.ResponseClientJtt809_0x9005;
import io.netty.channel.ChannelHandlerContext;

/**
 * 从链路连接保持请求消息
 * 链路类型:从链路。
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:DOWN_ LINKTEST_ REQ。
 * 描述:从链路建立成功后，上级平台向下级平台发送从链路连接保持请求消息，以保持
 * 从链路的连接状态。
 * 从链路连接保持请求消息，数据体为空。
 */
public class ResponseClientHandlerImpl_0x9005 implements IClientBusinessServer<ResponseClientJtt809_0x9005> {

    public void businessHandler(ChannelHandlerContext ctx, ResponseClientJtt809_0x9005 msg) {
        ctx.writeAndFlush(new RequestClientJtt809_0x9006());
    }
}
