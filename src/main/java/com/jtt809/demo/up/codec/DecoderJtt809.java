package com.jtt809.demo.up.codec;

import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BasePackage;
import com.jtt809.demo.up.pojo.Response;
import com.jtt809.demo.up.pojo.ResponseFactory;
import com.jtt809.demo.up.util.HexBytesUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * <p>jtt809解码类</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 17:04
 */
@Slf4j
public class DecoderJtt809 extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        try {
            short startByte = in.getByte(0);
            //粘包后导致的半包数据不要读
            if (BasePackage.MSG_HEAD_FLAG != startByte) {
                return;
            }

            // region 报文转义
            ByteBuf tempByteBuf = Unpooled.copiedBuffer(in);
            ByteBuf finalBytebuf = Unpooled.buffer();
            BasePackage.messageInversion(tempByteBuf.array(), finalBytebuf);

            ReferenceCountUtil.release(tempByteBuf);
            tempByteBuf = Unpooled.copiedBuffer(finalBytebuf);
            byte[] bytesTemp = tempByteBuf.array();
            ReferenceCountUtil.release(tempByteBuf);
            log.info("======================> 上级平台报文转义 : {}", HexBytesUtil.bytesToHex(bytesTemp));
            // end region

            //数据体解密
            short encryptFlag = finalBytebuf.getByte(ConstantJtt809.ENCRYPT_FLAG_INDEX_OF_PACKAGE);
            if (ConstantJtt809.ENCRYPT_FLAG_PACKAGE_Y == encryptFlag) {
                finalBytebuf = BasePackage.decrypt(finalBytebuf);
            }

            int msgId = finalBytebuf.getUnsignedShort(ConstantJtt809.MSG_ID_INDEX_OF_PACKAGE);
            try {
                log.info("===========> 上级平台解码的类型为 0x{} ", Integer.toHexString(msgId));
                // 工厂根据传入的指令实例化对象
                Response response = ResponseFactory.createResponse(msgId, finalBytebuf);
                if (null != response) {
                    out.add(response);
                }
            } finally {
                ReferenceCountUtil.release(finalBytebuf);
                in.clear();
            }
        } catch (Exception e) {
            log.error("===========> 上级平台编码异常", e);
        }
    }


}
