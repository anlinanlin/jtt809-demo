package com.jtt809.demo.up.handler.initializer;

import com.jtt809.demo.up.codec.DecoderJtt809;
import com.jtt809.demo.up.codec.EncoderJtt809;
import com.jtt809.demo.up.handler.PrimaryLinkServerJtt809Handler;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * <p>主链路初始化</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 16:57
 */
@Slf4j
public class PrimaryLinkServerJtt809Initializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline channelPipeline = ch.pipeline();

        //粘包分隔符
        final ByteBuf delimiter = Unpooled.buffer(1);
        delimiter.writeByte(BasePackage.MSG_END_FLAG);
        //没有分隔符的缓存的数据包超出maxFrameLength长度会被丢弃，frame length exceeds xx: xxx - discarded
        channelPipeline.addLast("delimiter", new DelimiterBasedFrameDecoder(1024, delimiter));

        // 打印日志信息
//        channelPipeline.addLast("loging", new LoggingHandler(LogLevel.INFO));

        // 解码 和 编码
        channelPipeline.addLast("decoder", new DecoderJtt809());
        channelPipeline.addLast("encoder", new EncoderJtt809());

        // 心跳检测，每隔150s检测一次是否要读事件，如果超过150s你没有读事件的发生，则执行相应的操作
        channelPipeline.addLast("timeout", new IdleStateHandler(150, 0, 0, TimeUnit.SECONDS));

        // 业务逻辑Handler
        channelPipeline.addLast("handler", new PrimaryLinkServerJtt809Handler());
    }

}
