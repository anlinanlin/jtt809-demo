package com.jtt809.demo.up.pojo.command.response;

import com.jtt809.demo.up.pojo.Response;
import io.netty.buffer.ByteBuf;

/**
 * 从链路连接保持应答消息
 * 链路类型：从链路:
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:DOWN_ LINKTEST_ REP。
 * 描述:下级平台收到上级平台链路连接保持请求消息后，向上级平台返回从链路连接保
 * 持应答消息，保持从链路连接状态。
 * 从链路连接保持应答消息，数据体为空。
 */
public class ResponseJtt809_0x9006 extends Response {
    @Override
    protected void decodeImpl(ByteBuf buf) {

    }
}
