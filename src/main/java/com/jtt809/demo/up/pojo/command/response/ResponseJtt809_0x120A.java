package com.jtt809.demo.up.pojo.command.response;

import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import lombok.Data;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 上报车辆驾驶员身份识别信息应答
 * 子业务类型标识：UP_EXG_MSG_REPORT_DRIVER_INFO_ACK
 * 描述：上报车辆驾驶员身份识别信息应答
 */
@Data
public class ResponseJtt809_0x120A extends ResponseJtt809_0x1200_VehiclePackage {

    /**
     * 驾驶员姓名
     */
    private String driverName;

    /**
     * 驾驶员身份证编号
     */
    private String driverId;

    /**
     * 从业资格证号(备用)
     */
    private String licence;

    /**
     * 发证机构名称(备用)
     */
    private String orgName;


    /*2019*/
    /**
     * 上报信息源子业务类型
     */
    private short sourceDataType;
    /**
     * 上报信息源序列号
     */
    private int sourceMsgSn;
    /**
     * 证件有效期，时分秒为0
     */
    private long validDate;
    /*2019*/

    @Override
    protected void decodeDataImpl(ByteBuf buf) {
        if (isJtt809Version2019()) {
            this.sourceDataType = buf.readShort();
            this.sourceMsgSn = buf.readInt();
        }

        this.driverName = buf.readBytes(16).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
        this.driverId = buf.readBytes(20).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
        this.licence = buf.readBytes(40).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
        this.orgName = buf.readBytes(200).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();

        if (isJtt809Version2019()) {
            this.validDate = buf.readLong();
        }
    }
}
